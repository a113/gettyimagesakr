//
//  ImageElement.swift
//  GettyImagesAkr
//
//  Created by Andriy Kruglyanko on 11/28/18.
//  Copyright © 2017 myself. All rights reserved.
//

import UIKit
import RealmSwift

class ImageElement: Object {
    @objc dynamic var searchText = ""
    @objc dynamic var date = NSDate()
    @objc dynamic var dataPNGImg = Data()
}
