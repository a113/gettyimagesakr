//
//  MainViewController+NSURLSessionTaskDelegate.swift
//  GettyImagesAkr
//
//  Created by Andriy Kruglyanko on 11/28/18.
//  Copyright © 2017 myself. All rights reserved.
//

import UIKit

extension MainViewController: URLSessionDataDelegate {

    func urlSession(_ session: URLSession,
                             task: URLSessionTask,
                             didCompleteWithError error: Error?){
        NSLog("error didCompleteWithError = %@",error?.localizedDescription ?? "")
        
        if (error == nil)
        {
            NSLog("Task: %@ completed successfully", task)
        }
        else
        {
            NSLog("Task: %@ completed with error: %@", task, error?.localizedDescription ?? "")
        }

    }
    
}
