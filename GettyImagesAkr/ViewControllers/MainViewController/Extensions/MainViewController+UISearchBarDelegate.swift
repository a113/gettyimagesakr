//
//  MainViewController+UISearchBarDelegate.swift
//  GettyImagesAkr
//
//  Created by Andriy Kruglyanko on 11/28/18.
//  Copyright © 2017 myself. All rights reserved.
//

import UIKit

extension MainViewController : UISearchBarDelegate {
    
    // MARK: - UISearchBarDelegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        NSLog("The default search selected scope button index changed to \(selectedScope).")
    }
    
    func  searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        NSLog("The default search bar keyboard search button was tapped: \(String(describing: searchBar.text)).")
        let cur:String = (searchBar.text) ?? ""
        let curString : String = "\(MainViewController.getImages)\(cur)"
        if let urlNewStr = curString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
            let curUrl  = NSURL(string: urlNewStr) {
            loadResponse(Url: curUrl as URL , fileName : "response")
        }
        
        let aString = searchBar.text
        _ = aString?.replacingOccurrences(of: " ", with: "+")
        
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        NSLog("The default search bar cancel button was tapped.")
        
        searchBar.resignFirstResponder()
    }
}
