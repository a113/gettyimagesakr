//
//  MainViewController+UITableViewDelegate.swift
//  GettyImagesAkr
//
//  Created by Andriy Kruglyanko on 11/28/18.
//  Copyright © 2017 myself. All rights reserved.
//

import UIKit

extension MainViewController: UITableViewDataSource, UITableViewDelegate {

    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Cell
        let object = results[indexPath.row]
        cell.textLabel?.text = object.searchText
        cell.detailTextLabel?.text = object.date.description
        cell.imageView?.image = UIImage(data: object.dataPNGImg)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath)
    {
        
        let detailController = DetailViewController()
        let object = results[indexPath.row]
        detailController.dataImg = object.dataPNGImg
        self.navigationController?.pushViewController(detailController, animated: true)
        
    }
}
