//
//  MainViewController.swift
//  GettyImagesAkr
//
//  Created by Andriy Kruglyanko on 11/28/18.
//  Copyright © 2017 myself. All rights reserved.
//

import UIKit
import RealmSwift

class Cell: UITableViewCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init(coder: NSCoder) {
        fatalError("NSCoding not supported")
    }
}

class MainViewController: UIViewController, URLSessionTaskDelegate {
    
    // MARK: Publics
    
    let realm = try! Realm()
    let results = try! Realm().objects(ImageElement.self).sorted(byKeyPath: "date", ascending: false)
    var notificationToken: NotificationToken?
    static let apiKey = "any9pvhnyf6wn9cdjrbspvv3"
    static let getImages = //"https://api.gettyimages.com/v3/search/images?ﬁelds=id,title,thumb&sort_order=best&phrase="
    "https://api.gettyimages.com/v3/search/images/creative?phrase="
    static let heightSearchBar: CGFloat = 56
    
    // MARK: Privates
    
    private var searchBar: UISearchBar!
    private var myTableView: UITableView!
    
    // MARK: - View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = .top//.UIRectEdgeTop
        self.view.backgroundColor = UIColor.white
        self.title = "GettyImagesAkr"
        
        //myTableView = UITableView(frame: CGRect(x: 0, y: 120, width: self.view.frame.size.width, height: self.view.frame.size.height - 120))
myTableView = UITableView(frame: CGRect.zero)
        myTableView.translatesAutoresizingMaskIntoConstraints = false
        myTableView.backgroundColor = UIColor.green
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        myTableView.dataSource = self
        myTableView.delegate = self
        self.view.addSubview(myTableView)
        myTableView.register(Cell.self, forCellReuseIdentifier: "cell")
        
        
        searchBar = UISearchBar()
        searchBar.delegate = self
        if UIDevice.current.orientation.isLandscape {
            searchBar.frame = CGRect.zero//CGRect(x:0,y:0,width: self.view.frame.size.width,height: MainViewController.heightSearchBar)
            
        } else {
            searchBar.frame = CGRect.zero//CGRect(x:0,y:64,width: self.view.frame.size.width,height: MainViewController.heightSearchBar)
        }
        searchBar.frame.size.height = MainViewController.heightSearchBar
        
        // add shadow
        searchBar.layer.shadowColor = UIColor.black.cgColor
        searchBar.layer.shadowOpacity = 0.5
        searchBar.layer.masksToBounds = false
        
        // hide cancel button
        searchBar.showsCancelButton = true
        
        // hide bookmark button
        searchBar.showsBookmarkButton = false
        
        // set Default bar status.
        searchBar.searchBarStyle = UISearchBarStyle.default
 
        // set placeholder
        searchBar.placeholder = "Input text"
        
        // change the color of cursol and cancel button.
        searchBar.tintColor = UIColor.red
        
        // hide the search result.
        searchBar.showsSearchResultsButton = false
        self.view.addSubview(searchBar)
        
        self.constraintsSettings()
        // Set results notification block
        //self.notificationToken = results.observe() {[weak self] (changes: RealmCollectionChange) in
        self.notificationToken = results.observe { [weak self] (changes: RealmCollectionChange) in
            switch changes {
            case .initial:
                // Results are now populated and can be accessed without blocking the UI
                self?.myTableView.reloadData()
                break
            case .update(_, let deletions, let insertions, let modifications):
                // Query results have changed, so apply them to the TableView
                self?.myTableView.beginUpdates()
                self?.myTableView.insertRows(at: insertions.map { IndexPath(row: $0, section: 0) }, with: .automatic)
                self?.myTableView.deleteRows(at: deletions.map { IndexPath(row: $0, section: 0) }, with: .automatic)
                self?.myTableView.reloadRows(at: modifications.map { IndexPath(row: $0, section: 0) }, with: .automatic)
                self?.myTableView.endUpdates()
                break
            case .error(let err):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(err)")
                break
            }
        }
        self.view.setNeedsDisplay()
        NotificationCenter.default.addObserver(self, selector: #selector(self.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        let clearItem = UIBarButtonItem(title: "Clear", style: .plain, target: self, action: #selector(trashActionDeletaLocalDatabase))
        self.navigationItem.rightBarButtonItem = clearItem

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //let curUrl  = NSURL(string: "https://api.gettyimages.com/v3/search/images?phrase=kitties")
        //loadResponse(Url: curUrl! as URL , fileName : "response")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - Memory management
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action Handlers
    
    @objc func rotated() {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape \(self.searchBar.frame), \(self.myTableView.frame), \(String(describing: self.navigationController?.navigationBar.frame))")
            searchBar.frame = CGRect(x:0,y:64,width: self.view.frame.size.width,height:MainViewController.heightSearchBar)
            
        } else {
            print("Portrait \(String(describing: self.searchBar?.frame)), \(self.myTableView.frame), \(String(describing: self.navigationController?.navigationBar.frame))")
            searchBar.frame = CGRect(x:0,y:64,width:self.view.frame.size.width,height:MainViewController.heightSearchBar)
        }
        //constraintsSettings()
        self.view.setNeedsLayout()
    }
    
    @objc func trashActionDeletaLocalDatabase(_ sender: Any)    {
        try! realm.safeWrite {
            realm.deleteAll()
        }
        
    }
    
    // MARK: - Public
    
    func loadResponse(Url: URL  , fileName : String) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let request = NSMutableURLRequest(url: Url )
        request.httpMethod = "GET"
        request.addValue(MainViewController.apiKey, forHTTPHeaderField: "Api-Key")
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true;
        //let task = session.dataTask(with: request) { (data, response, error) in
        let task = session.dataTask(with: request as URLRequest){ (data, response, error) in
        // check for any errors
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error!)
                DispatchQueue.main.async {
                    self.showErrorDownload(error: error!)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false;
                }
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false;
                }
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [String: Any] else {
                        print("error trying to convert data to JSON")
                        DispatchQueue.main.async {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false;
                        }
                        return
                }
                // now we have the todo
                // let's just print it to prove we can access it
                print("The todo is: " + todo.description)
                
                
                guard let todoImages = todo["images"] as? Array<Any> else {
                    guard let message = todo["message"] as? String else {
                        DispatchQueue.main.async {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false;
                        }
                        return
                    }
                    DispatchQueue.main.async {
                        self.showSimpleAlert(text: message)
                    }
                    DispatchQueue.main.async {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false;
                    }

                    print("Could not get todo images from JSON")
                    return
                }
                
                if(todoImages.count > 0)
                {
                    if let todoImages0 = (todoImages[0]) as? Dictionary<String, Any>,
                        let display_sizes = todoImages0["display_sizes"] as? Array<Any> ,
                        let display_sizes0 = (display_sizes[0]) as? Dictionary<String, Any>,
                        let urlImage = display_sizes0["uri"] as? String
                     {
                        DispatchQueue.main.async {
                            let curUrl  = NSURL(string: urlImage)
                            self.loadImage(Url: (curUrl as URL?)!)
                        }
                    }
                } else
                {
                    DispatchQueue.main.async {
                        self.showSimpleAlert(text: "No images found ")
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false;
                    }
                }
                
                print("The images is: \(todoImages)")
            } catch  {
                print("error trying to convert data to JSON")
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false;
                }
                return
            }
        }
        task.resume()
}
    
    @objc func add(dataPngImg: Data, serchText: String) {
        realm.beginWrite()
        realm.create(ImageElement.self, value: [serchText, NSDate(), dataPngImg])
        try! realm.commitWrite()
    }
    
    /// Show an alert with an "Okay" button.
    func showSimpleAlert(text: String) {
        let title = NSLocalizedString("", comment: "")
        let message = NSLocalizedString(text, comment: "")
        let cancelButtonTitle = NSLocalizedString("OK", comment: "")
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // Create the action.
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel) { action in
            NSLog("The simple alert's cancel action occured.")
        }
        
        // Add the action.
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func showErrorDownload(error: Error) {
        let title = NSLocalizedString("", comment: "")
        let message = NSLocalizedString(error.localizedDescription, comment: "")
        let cancelButtonTitle = NSLocalizedString("OK", comment: "")
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        // Create the action.
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel) { action in
            NSLog("The simple alert's cancel action occured.")
        }
        
        // Add the action.
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func loadImage(Url: URL)
    {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let request = NSMutableURLRequest(url: Url )
        request.httpMethod = "GET"
        
        request.addValue(MainViewController.apiKey, forHTTPHeaderField: "Api-Key")
        let task = session.dataTask(with: request as URLRequest){ (data, response, error) in
            // check for any errors
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error!)
                DispatchQueue.main.async {
                    self.showErrorDownload(error: error!)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false;
                }
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false;
                }
                return
            }
            // parse the result as JSON, since that's what the API provides
            if let imgPNG = UIImage(data: responseData),
                let dataPNGImg = UIImagePNGRepresentation(imgPNG) as Data?
            {
                DispatchQueue.main.async {
                    self.add(dataPngImg: dataPNGImg as Data, serchText: self.searchBar.text!)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false;
                }
            }
        }
        task.resume()
        
    }
    
    func constraintsSettings()
    {
        self.searchBar.translatesAutoresizingMaskIntoConstraints = false
        
        
        NSLayoutConstraint(item: searchBar, attribute: .height, relatedBy: .equal, toItem: nil, attribute:.notAnAttribute, multiplier: 1.0, constant:MainViewController.heightSearchBar).isActive = true
        NSLayoutConstraint(item: searchBar, attribute: .leading, relatedBy: .equal, toItem: self.view.safeAreaLayoutGuide, attribute:.leadingMargin, multiplier: 1.0, constant:0.0).isActive = true
        NSLayoutConstraint(item: searchBar, attribute: .trailing, relatedBy: .equal, toItem: self.view.safeAreaLayoutGuide, attribute:.trailingMargin, multiplier: 1.0, constant:0.0).isActive = true
        NSLayoutConstraint(item: searchBar, attribute: .top, relatedBy: .equal, toItem: self.view.safeAreaLayoutGuide, attribute:.topMargin, multiplier: 1.0, constant:0).isActive = true
        NSLayoutConstraint(item: myTableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute:.leading, multiplier: 1.0, constant:0.0).isActive = true
        NSLayoutConstraint(item: myTableView, attribute: .top, relatedBy: .equal, toItem: searchBar, attribute:.bottom, multiplier: 1.0, constant:0.0).isActive = true
        NSLayoutConstraint(item: myTableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute:.trailing, multiplier: 1.0, constant:0.0).isActive = true
        NSLayoutConstraint(item: myTableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute:.bottom, multiplier: 1.0, constant:0.0).isActive = true
        self.view.setNeedsLayout()
    }
    
    // MARK: - Private

    
    
}
