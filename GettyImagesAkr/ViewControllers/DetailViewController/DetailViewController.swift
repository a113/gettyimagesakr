//
//  DetailViewController.swift
//  GettyImagesAkr
//
//  Created by Andriy Kruglyanko on 11/28/18.
//  Copyright © 2017 myself. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    // MARK: Publics
    public var dataImg: Data!
    
    // MARK: Privates
    private var imgView: UIImageView!
    
// MARK: - View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imgView = UIImageView(frame:CGRect(x: 0, y: (self.navigationController?.navigationBar.frame.height)!, width: self.view.frame.width, height: self.view.frame.height - (self.navigationController?.navigationBar.frame.height)!))
        imgView.contentMode = UIViewContentMode.scaleAspectFit
        self.view.addSubview(imgView)
        self.view.setNeedsDisplay()
        NotificationCenter.default.addObserver(self, selector: #selector(self.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        imgView.image = UIImage(data:dataImg)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Memory management

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Action Handlers
    
    // MARK: - Public
    
    func constraintsSettings()
    {
        self.imgView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint(item: imgView, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute:.leadingMargin, multiplier: 1.0, constant:0.0).isActive = true
        NSLayoutConstraint(item: imgView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute:.trailingMargin, multiplier: 1.0, constant:0.0).isActive = true
        NSLayoutConstraint(item: imgView, attribute: .top, relatedBy: .equal, toItem: self.view.safeAreaLayoutGuide, attribute:.topMargin, multiplier: 1.0, constant:0).isActive = true
        NSLayoutConstraint(item: imgView, attribute: .bottom, relatedBy: .equal, toItem: self.view.safeAreaLayoutGuide, attribute:.bottomMargin, multiplier: 1.0, constant:0).isActive = true
    }
    
    
    
    @objc func rotated() {
        if (UIDevice.current.userInterfaceIdiom == .phone) {
            if UIDevice.current.orientation.isLandscape {
                print("Landscape im \(self.imgView.frame),\(String(describing: self.navigationController?.navigationBar.frame))")
                imgView.frame = CGRect(x:0,y:(self.navigationController?.navigationBar.frame.height)!,width:self.view.frame.size.width,height:self.view.frame.height - (self.navigationController?.navigationBar.frame.height)! )
            } else {
                print("Portrait im \(String(describing: self.imgView?.frame)), \(self.imgView.frame), \(String(describing: self.navigationController?.navigationBar.frame))")
                imgView.frame = CGRect(x:0,y:(self.navigationController?.navigationBar.frame.height)!,width:self.view.frame.size.width, height:self.view.frame.height - (self.navigationController?.navigationBar.frame.height)!)
                
            }
        } else {
            if UIDevice.current.orientation.isLandscape {
                print("Landscape im \(self.imgView.frame),\(String(describing: self.navigationController?.navigationBar.frame))")
                imgView.frame = CGRect(x:0,y:(self.navigationController?.navigationBar.frame.height)!,width:self.view.frame.height,height:self.view.frame.size.width)
            } else {
                print("Portrait im \(String(describing: self.imgView?.frame)), \(self.imgView.frame), \(String(describing: self.navigationController?.navigationBar.frame))")
                imgView.frame = CGRect(x:0,y:(self.navigationController?.navigationBar.frame.height)!,width:self.view.frame.height,height:self.view.frame.size.width)
                
            }
        }
        self.view.setNeedsLayout()
        self.view.setNeedsDisplay()
        self.imgView.setNeedsLayout()
        self.imgView.setNeedsDisplay()
    }
    
    // MARK: - Private
    
    
    
    // MARK: - Delegates
    
    // MARK: - Navigation
    

    
}
